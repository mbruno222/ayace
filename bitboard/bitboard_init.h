#ifndef BITBOARD_INIT_H
#define BITBOARD_INIT_H

#include <stdint.h>
#include "bitboard.h"

extern bitboard rank_attacks[64][64]; // 32 kilobytes
extern bitboard file_attacks[64][64];
extern bitboard diaga1h8_attacks[64][64];
extern bitboard diaga8h1_attacks[64][64];

extern bitboard knight_attacks[64];
extern bitboard king_attacks[64];
extern bitboard pawn_attacks[2][64];
extern bitboard pawn_moves[2][64];

extern bitboard piece_setmask[64];
extern bitboard piece_setmask_rl90[64];
extern bitboard piece_setmask_rl45[64];
extern bitboard piece_setmask_rr45[64];

extern int coord_map_rl90[64];
extern int coord_map_rl45[64];
extern int coord_map_rr45[64];

board_t clear_board();
board_t reset_board();
void bitboard_system_init();

#endif

