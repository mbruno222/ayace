#include <stdlib.h>
#include <stdio.h>
#include "../common.h"
#include "bitboard.h"
#include "bitboard_init.h"

void display_bitboard(FILE *of, bitboard b)
{
	int r, f;
	uint8_t rank;

	fprintf(of, "   A B C D E F G H\n");
	fprintf(of, "  -----------------  \n");

	for (r=0; r<8; r++) {
		rank = b >> ((7-r)<<3); // start with rank 8, work down to rank 1
		fprintf(of, "%d ", 8-r);
		for (f=0; f<8; f++) {
			fprintf(of, "|%c", (rank & 0x01) ? 'X' : ' '); // file A to H == bit 0 to 7
			rank >>= 1;
		}
		fprintf(of, "| %d\n  -----------------\n", 8-r);
	}
	fprintf(of, "   A B C D E F G H\n");
}

void print_board(FILE *of, board_t *b)
{
	int r, f, sq;
	int piece, color;
	const char wp[8] = {' ', 'P', 'N', 'K', ' ', 'B', 'R', 'Q'};
	const char bp[8] = {' ', 'p', 'n', 'k', ' ', 'b', 'r' ,'q'};

	fprintf(of, "   A B C D E F G H\n");
	fprintf(of, "  -----------------  \n");

	for (r=7; r>=0; r--) {
		fprintf(of, "%d ", r+1);
		for (f=0; f<8; f++) {
			sq = C2N(r,f);
			color = get_piece_color(b, sq);
			piece = get_piece_type(b, sq, color);
			fprintf(of, "|%c", color == WHITE ? wp[piece] : bp[piece]);
		}
		fprintf(of, "| %d\n  -----------------\n", r+1);
	}
	fprintf(of, "   A B C D E F G H\n");
}
