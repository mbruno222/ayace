#include <stdlib.h>
#include <stdio.h>
#include "../common.h"
#include "bitboard.h"
#include "bitboard_init.h"
#include "bitboard_movelist.h"
#include "../hash.h"

#define CAPTURES   1
#define PROMOTIONS 2
#define QUIET      3

// color is color of attacking pieces,
// not of square being attacked
bitboard get_attacks_to(board_t *b, int sq, int color)
{
	bitboard attacks, all_attacks;
	uint8_t contents;
	
	// find attacks on sq from diagonal sliders
	contents = get_diag_contents(b->all_pieces_rl45, sq, coord_map_rl45);
	attacks = diaga8h1_attacks[sq][(contents & 0x7F) >> 1];
	contents = get_diag_contents(b->all_pieces_rr45, sq, coord_map_rr45);
	attacks |= diaga1h8_attacks[sq][(contents & 0x7F) >> 1];
	attacks &= (b->color[color].bishops | b->color[color].queens);
	all_attacks = attacks;


	// find attacks on sq from rect sliders
	contents = get_rank_contents(b->all_pieces, sq);
	attacks = rank_attacks[sq][(contents & 0x7F) >> 1];
	contents = get_file_contents(b->all_pieces_rl90, sq);
	attacks |= file_attacks[sq][(contents & 0x7F) >> 1];
	attacks &= (b->color[color].rooks | b->color[color].queens);
	all_attacks |= attacks;

	all_attacks |= (knight_attacks[sq] & b->color[color].knights);
	all_attacks |= (king_attacks[sq] & b->color[color].king);
	all_attacks |= (pawn_attacks[!color][sq] & b->color[color].pawns);

	return all_attacks;
}

bitboard get_attacks_from(board_t *b, int sq, int piece, int color)
{
	bitboard attacks = 0;
	uint8_t contents;

	switch (piece) {
	case BISHOP:
	case ROOK:
	case QUEEN:
		if (BISHOP_MOVER(piece)) {
			contents = get_diag_contents(b->all_pieces_rl45, sq, coord_map_rl45);
			attacks |= diaga8h1_attacks[sq][(contents & 0x7F) >> 1];
			contents = get_diag_contents(b->all_pieces_rr45, sq, coord_map_rr45);
			attacks |= diaga1h8_attacks[sq][(contents & 0x7F) >> 1];
		}
		if (ROOK_MOVER(piece)) {
			contents = get_rank_contents(b->all_pieces, sq);
			attacks |= rank_attacks[sq][(contents & 0x7F) >> 1];
			contents = get_file_contents(b->all_pieces_rl90, sq);
			attacks |= file_attacks[sq][(contents & 0x7F) >> 1];
		}
		break;
	case KNIGHT:
		attacks = knight_attacks[sq];
		break;
	case KING:
		attacks = king_attacks[sq];
		break;
	case PAWN:
		attacks = pawn_attacks[color][sq];
		break;
	default:
		break;
	}

	return attacks;
}

bitboard get_pawn_pushes(board_t *b, int sq, int color)
{
	bitboard pushes;

	pushes = pawn_moves[color][sq] & ~(b->all_pieces);
	if (color == WHITE && (sq & 0xF8) == 8 && (piece_setmask[sq+8] & b->all_pieces)) {
		pushes &= ~piece_setmask[sq+16];
	} else if (color == BLACK && (sq & 0xF8) == 48 && (piece_setmask[sq-8] & b->all_pieces)) {
		pushes &= ~piece_setmask[sq-16];
	}

	return pushes;
}

static int find_castles(board_t *b, move_t move_list[MOVE_LIST_MAX], int color)
{
	int count = 0;
	if (b->kscastle[color]) {
		if (color == WHITE &&
		    (b->all_pieces & (piece_setmask[5] | piece_setmask[6])) == 0 &&
		    !get_attacks_to(b, 4, BLACK) && !get_attacks_to(b, 5, BLACK) && !get_attacks_to(b, 6, BLACK)) {
			count++;
			move_list->moving_piece = KING;
			move_list->from = 4;
			move_list->to = 6;
			move_list->captured_piece = 0;
			move_list->new_piece = 0;
			move_list++;
		} else if (color == BLACK &&
		    (b->all_pieces & (piece_setmask[5+56] | piece_setmask[6+56])) == 0 &&
		    !get_attacks_to(b, 4+56, WHITE) && !get_attacks_to(b, 5+56, WHITE) && !get_attacks_to(b, 6+56, WHITE)) {
			count++;
			move_list->moving_piece = KING;
			move_list->from = 4+56;
			move_list->to = 6+56;
			move_list->captured_piece = 0;
			move_list->new_piece = 0;
			move_list++;
		}
	}

	if (b->qscastle[color]) {
		if (color == WHITE &&
		    (b->all_pieces & (piece_setmask[1] | piece_setmask[2] | piece_setmask[3])) == 0 &&
		    !get_attacks_to(b, 2, BLACK) && !get_attacks_to(b, 3, BLACK) && !get_attacks_to(b, 4, BLACK)) {
			count++;
			move_list->moving_piece = KING;
			move_list->from = 4;
			move_list->to = 2;
			move_list->captured_piece = 0;
			move_list->new_piece = 0;
			move_list++;
		} else if (color == BLACK &&
		    (b->all_pieces & (piece_setmask[1+56] | piece_setmask[2+56] | piece_setmask[3+56])) == 0 &&
		    !get_attacks_to(b, 2+56, WHITE) && !get_attacks_to(b, 3+56, WHITE) && !get_attacks_to(b, 4+56, WHITE)) {
			count++;
			move_list->moving_piece = KING;
			move_list->from = 4+56;
			move_list->to = 2+56;
			move_list->captured_piece = 0;
			move_list->new_piece = 0;
			move_list++;
		}
	}

	return count;
}

static int find_moves(board_t *b, move_t move_list[MOVE_LIST_MAX], int move_type, int color)
{
	int i, move_count = 0;
	int from, to, piece;
	bitboard pb, attacks;

	piece = PAWN;
	for (i=0; i<6; i++) {
		pb = get_piece_board(b, color, piece);

		if (move_type != CAPTURES && piece == KING) {
			int c;
			
			c = find_castles(b, move_list, color);
			move_count += c;
			move_list += c;
		}

		while (pb) {
			from = last_one_64(pb);

			if (piece == PAWN && move_type != CAPTURES) {
				/* for pawns, get_attacks_from() only returns capture moves, not pushes */
				attacks = get_pawn_pushes(b, from, color);
			} else {
				attacks = get_attacks_from(b, from, piece, color);
				if (move_type == CAPTURES) {
					/*
					 * exclude moves to empty pawn shadow squares
					 * when the moving piece is not a pawn and we're
					 * looking for captures.
					 */
					attacks &= ~(b->color[!color].pawn_shadow);
				}
			}

			switch (move_type) {
			case CAPTURES:
				/*
				 * when looking for captures, only consider
				 * moves that end on an opponent's piece
				 * or a pawn shadow for en passants.
				 */
				attacks &= b->color[!color].all | b->color[!color].pawn_shadow;
				break;
			default:
				/*
				 * for non-captures, only consider moves that
				 * end on empty squares.
				 */
				attacks &= ~(b->all_pieces);
			}

			while (attacks) {
				to = last_one_64(attacks);
				move_list->from = from;
				move_list->to = to;
				move_list->moving_piece = piece;
				if (piece == PAWN && (piece_setmask[to] & b->color[!color].pawn_shadow)) {
					move_list->captured_piece = PAWN;
				} else {
					move_list->captured_piece = get_piece_type(b, to, !color);
				}
				if (piece == PAWN && ((to>>3) == 7 || (to>>3) == 0)) {
					move_list->new_piece = QUEEN;
					
					move_list++;
					move_count++;
		
					// also consider underpromotion to knight
					move_list->from = from;
					move_list->to = to;
					move_list->moving_piece = piece;
					move_list->captured_piece = (move_list-1)->captured_piece;
					move_list->new_piece = KNIGHT;

				} else {
					move_list->new_piece = 0;
				}
				move_list++;
				move_count++;
				attacks = BITCLEAR(attacks, to);
			}
			pb = BITCLEAR(pb, from);
		}

		switch (i) {
		case 0:
			piece = ROOK;
			break;
		case 1:
			piece = KNIGHT;
			break;
		case 2:
			piece = BISHOP;
			break;
		case 3:
			piece = QUEEN;
			break;
		case 4:
			piece = KING;
			break;
		default:
			pb = 0;
			break;
		}
	}

	return move_count;
}

int get_capture_list(board_t *b, move_t move_list[MOVE_LIST_MAX], int color)
{
	return find_moves(b, move_list, CAPTURES, color);
}

int get_quiet_move_list(board_t *b, move_t move_list[MOVE_LIST_MAX], int color)
{
	return find_moves(b, move_list, QUIET, color);
}

int get_move_list(board_t *b, move_t move_list[MOVE_LIST_MAX], int color)
{
	int move_count;
	
	// find captures FIRST
	move_count = find_moves(b, move_list, CAPTURES, color);
	move_list += move_count;

	// find the rest
	move_count += find_moves(b, move_list, QUIET, color);

	return move_count;
}

/*
 * Moves the move at index to the back of the move list
 */
void deprioritize_move(move_t move_list[MOVE_LIST_MAX], int move_count, int index)
{
	move_t m;

	if (index >= move_count-1) {
		return;
	}

	m = move_list[index];

	for (; index < move_count-1; index++) {
		move_list[index] = move_list[index+1];
	}
	move_list[move_count-1] = m;
}

/*
 * Moves the move at index to the front of the move list
 */
void prioritize_move(move_t move_list[MOVE_LIST_MAX], int index)
{
	move_t m;

	if (index <= 0) {
		return;
	}

	m = move_list[index];

	for (; index > 0; index--) {
		move_list[index] = move_list[index-1];
	}
	move_list[0] = m;
}

int move_cmp(move_t m1, move_t m2)
{
	if (m1.to == m2.to && m1.from == m2.from && m1.new_piece == m2.new_piece) {
		return 1;
	} else {
		return 0;
	}
}

int in_list(move_t move_list[MOVE_LIST_MAX], int move_count, move_t m, int *index)
{
	int i;

	for (i=0; i<move_count; i++) {
		if (move_cmp(m, *move_list)) {
			if (index != NULL) {
				*index = i;
			}
			return 1;
		}
		move_list++;
	}

	return 0;
}

int no_legal_moves(board_t *b, move_t move_list[MOVE_LIST_MAX], int move_count, int color)
{
	int i;
	board_t b2;

	for (i=0; i<move_count; i++) {
		b2 = *b;
		if (!move_piece(b, *move_list++, color)) {
			*b = b2;
			return 0;
		}
	}

	return 1;
}

/*
 * Makes the move specified by m for the specified
 * color on the board b. Other than that it is
 * allowed to put the moving color into check,
 * it MUST be a legal move. If the move does put
 * the moving color into check then the move is
 * not made and 1 is returned. Otherwise 0 is
 * returned.
 */
int move_piece(board_t *b, move_t m, int color)
{
	board_t old_b;
	bitboard *mover, *opponent;
	int from, to;

	from = m.from;
	to = m.to;

	old_b = *b;

	// no more castles allowed if king is moved
	if (m.moving_piece == KING)
		b->qscastle[color] = b->kscastle[color] = 0;

	// no more castle allowed if rook is moved
	if (m.moving_piece == ROOK && (from == 0 || from == 56)) {
		b->qscastle[color] = 0;
	} else if (m.moving_piece == ROOK && (from == 7 || from == 63)) {
		b->kscastle[color] = 0;
	}

	// no more castle allowed if rook is captured
	if (m.captured_piece == ROOK && (to == 0 || to == 56)) {
		b->qscastle[!color] = 0;
	} else if (m.captured_piece == ROOK && (to == 7 || to == 63)) {
		b->kscastle[!color] = 0;
	}
	
	mover = get_piece_board_p(b, color, m.moving_piece);
	
	if (!m.new_piece) {
		*mover ^= piece_setmask[from] | piece_setmask[to];
	} else {
		// pawn promotion
		*mover ^= piece_setmask[from];
		mover = get_piece_board_p(b, color, m.new_piece);
		*mover |= piece_setmask[to];
	}

	b->color[color].all ^= piece_setmask[from] | piece_setmask[to];

	if (!m.captured_piece) {
		b->all_pieces ^= piece_setmask[from] | piece_setmask[to];
		b->all_pieces_rl90 ^= piece_setmask_rl90[from] | piece_setmask_rl90[to];
		b->all_pieces_rl45 ^= piece_setmask_rl45[from] | piece_setmask_rl45[to];
		b->all_pieces_rr45 ^= piece_setmask_rr45[from] | piece_setmask_rr45[to];
	} else {
		opponent = get_piece_board_p(b, !color, m.captured_piece);

		if (piece_setmask[to] & b->color[!color].pawn_shadow) {
			// en passant capture is handled a bit differently
			int passant;

			if (color == WHITE)
				passant = to - 8;
			else
				passant = to + 8;

			*opponent ^= piece_setmask[passant];
			b->color[!color].all ^= piece_setmask[passant];

			b->all_pieces ^= piece_setmask[from] | piece_setmask[to] | piece_setmask[passant];
			b->all_pieces_rl90 ^= piece_setmask_rl90[from] | piece_setmask_rl90[to] | piece_setmask_rl90[passant];
			b->all_pieces_rl45 ^= piece_setmask_rl45[from] | piece_setmask_rl45[to] | piece_setmask_rl45[passant];
			b->all_pieces_rr45 ^= piece_setmask_rr45[from] | piece_setmask_rr45[to] | piece_setmask_rr45[passant];
		} else {
			*opponent ^= piece_setmask[to];
			b->color[!color].all ^= piece_setmask[to];
			
			b->all_pieces ^= piece_setmask[from];
			b->all_pieces_rl90 ^= piece_setmask_rl90[from];
			b->all_pieces_rl45 ^= piece_setmask_rl45[from];
			b->all_pieces_rr45 ^= piece_setmask_rr45[from];
		}
	}

	// pawn shadow only lasts for one turn
	b->color[color].pawn_shadow = 0x0LLU;
	// leave a pawn shadow if pawn moves two spaces
	if (m.moving_piece == PAWN && ABS(from - to) == 16) {
		if (color == WHITE)
			b->color[WHITE].pawn_shadow = piece_setmask[to-8];
		else
			b->color[BLACK].pawn_shadow = piece_setmask[to+8];
	}

	// move the rook if it's a castle
	if (m.moving_piece == KING) {
		int castle = 0;

		if (from == 4 && to == 6) {
			castle = 1;
			from = 7;
			to = 5;
		} else if (from == 4 && to == 2) {
			castle = 1;
			from = 0;
			to = 3;
		} else if (from == 4+56 && to == 6+56) {
			castle = 1;
			from = 7+56;
			to = 5+56;
		} else if (from == 4+56 && to == 2+56) {
			castle = 1;
			from = 0+56;
			to = 3+56;
		}
		
		if (castle) {
			b->color[color].rooks ^= piece_setmask[from] | piece_setmask[to];
			b->color[color].all ^= piece_setmask[from] | piece_setmask[to];
			b->all_pieces ^= piece_setmask[from] | piece_setmask[to];
			b->all_pieces_rl90 ^= piece_setmask_rl90[from] | piece_setmask_rl90[to];
			b->all_pieces_rl45 ^= piece_setmask_rl45[from] | piece_setmask_rl45[to];
			b->all_pieces_rr45 ^= piece_setmask_rr45[from] | piece_setmask_rr45[to];
		}
	}


	if (!get_attacks_to(b, last_one_64(b->color[color].king), !color)) {

		if (b->zobrist_key_index-- == 0) {
			b->zobrist_key_index = BITBOARD_KEY_HIST_LEN-1;
		}
		gen_zobrist_key(b, !color);

		return 0;
	} else {
		*b = old_b;
		return 1;
	}
}


