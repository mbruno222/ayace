#ifndef BITBOARD_H
#define BITBOARD_H

#include <stdint.h>
#include <stdio.h>

#define COORD(s) ((s)[0]-'a' + (((s)[1]-'1')<<3))
#define C2N(r,f) (((r)<<3) + (f))

#define BITSET(n,b) ((n) | (bitboard)1 << (b))
#define BITCLEAR(n,b) ((n) & ~((bitboard)1 << (b)))
#define BIT(n,b) ((n) & (bitboard)1 << (b))

#define SLIDER(p) ((p) & 4)
#define ROOK_MOVER(p) (SLIDER(p) && ((p) & 2))
#define BISHOP_MOVER(p) (SLIDER(p) && ((p) & 1))

#define PAWN 1
#define KNIGHT 2
#define KING 3
#define BISHOP 5
#define ROOK 6
#define QUEEN 7

#define WHITE 0
#define BLACK 1

/*
 * TODO: move following #defines as well
 * as board_t struct to another file?
 */

#define BITBOARD_KEY_HIST_LEN_LOG2 4
#define BITBOARD_KEY_HIST_LEN (1<<BITBOARD_KEY_HIST_LEN_LOG2)
#define BITBOARD_KEY_HIST_INDEX_MASK (BITBOARD_KEY_HIST_LEN - 1)

#define MAX_PLYS 32

typedef uint64_t bitboard;

typedef struct {
	bitboard all;
	bitboard pawns;
	bitboard pawn_shadow;
	bitboard rooks;
	bitboard knights;
	bitboard bishops;
	bitboard queens;
	bitboard king;
} bitboard_set;

typedef struct {
	unsigned int from : 6;
	unsigned int to : 6;
	unsigned int moving_piece : 3;
	unsigned int captured_piece : 3;
	unsigned int new_piece : 3;
	unsigned int : 0;
} move_t;

typedef struct {
	bitboard all_pieces;
	bitboard all_pieces_rl90;
	bitboard all_pieces_rl45;
	bitboard all_pieces_rr45;
	bitboard_set color[2];

	uint64_t zobrist_key[BITBOARD_KEY_HIST_LEN];
	int zobrist_key_index;

	int kscastle[2];
	int qscastle[2];

	int move_number;

	move_t pv[MAX_PLYS];
} board_t;

void display_bitboard(FILE *of, bitboard b);
void print_board(FILE *of, board_t *b);

#include "bitboard_inline.h"

#endif

