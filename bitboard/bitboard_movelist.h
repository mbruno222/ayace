#ifndef BITBOARD_MOVELIST_H
#define BITBOARD_MOVELIST_H

#include "bitboard.h"

/*
 * Supposedly the theoretical maximum number of
 * legal/semi-legal moves from any given position
 * is 220-225, so 256 should be safe
 */
#define MOVE_LIST_MAX 256

bitboard get_attacks_to(board_t *b, int sq, int color);
bitboard get_attacks_from(board_t *b, int sq, int piece, int color);
bitboard get_pawn_pushes(board_t *b, int sq, int color);
int get_capture_list(board_t *b, move_t move_list[MOVE_LIST_MAX], int color);
int get_quiet_move_list(board_t *b, move_t move_list[MOVE_LIST_MAX], int color);
int get_move_list(board_t *b, move_t move_list[MOVE_LIST_MAX], int color);
int no_legal_moves(board_t *b, move_t move_list[MOVE_LIST_MAX], int move_count, int color);
void deprioritize_move(move_t move_list[MOVE_LIST_MAX], int move_count, int index);
void prioritize_move(move_t move_list[MOVE_LIST_MAX], int index);
int move_cmp(move_t m1, move_t m2);
int in_list(move_t move_list[MOVE_LIST_MAX], int move_count, move_t m, int *index);
int move_piece(board_t *b, move_t m, int color);

#endif

