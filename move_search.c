/* Copyright 2010 Michael Bruno
 *
 * This file is part of AYACE
 *
 * AYACE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * AYACE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AYACE.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "bitboard/bitboard.h"
#include "bitboard/bitboard_init.h"
#include "bitboard/bitboard_movelist.h"
#include "chess.h"
#include "hash.h"
#include "board_evaluation.h"
#include "move_search.h"
#include "xboard_io.h"

#define KILLER_MOVES_PER_PLY 2

static move_t killer_move[MAX_PLYS][KILLER_MOVES_PER_PLY];


int node_get_score(board_t *b, int color, int search_depth, int alpha, int beta, int ply, int *evals);

static search_tree_node_score_t node_search(board_t *board, move_t move_list[MOVE_LIST_MAX], int move_count, int color, int depth, int initial_score, int alpha, int beta, int ply, int *evals)
{
	int i;
	int beta2 = beta;
	int moved = 0;
	search_tree_node_score_t node_score;

	node_score.repeat = 0;
	node_score.type = node_all;
	node_score.score = initial_score;

	for (i=0; i<move_count; i++) {
		int score;
		move_t m = *move_list++;

		board_t move_board = *board;
		if (move_piece(&move_board, m, color)) {
			/* move leaves king in check */
			continue;
		}

		/*
		 * check if this is a repeat move. if it is then don't consider it.
		 * move on to the next move, or if this was the only move then return
		 * that this is a cut node with score of beta (alpha for the caller)
		 */
		if (color == engine_color && get_zobrist_key(&move_board, 4) == get_zobrist_key(&move_board, 0)) {
			node_score.repeat = 1;
			if (move_count > 1) {
				/* if it is then don't consider it */
				continue;
			} else {
				node_score.type = node_cut;
				node_score.score = beta;
				return node_score;
			}
		}

		moved = 1;

		score = -node_get_score(&move_board, !color, depth-1, -beta2, -alpha, ply+1, evals);

		if (alpha < score && score < beta && beta2 != beta) {
			/* better to search with [-beta, -score] rather than [-beta, -alpha] */
			score = -node_get_score(&move_board, !color, depth-1, -beta, -score, ply+1, evals);
		}

		if (score >= beta) {
			node_score.score = score;
			node_score.type = node_cut;
			node_score.move = m;
			return node_score;
		} else if (score > node_score.score) {
			node_score.score = score;
			if (score > alpha) {
				alpha = score;
				beta2 = alpha+1;
				node_score.type = node_pv;
				node_score.move = m;
				memcpy(board->pv, move_board.pv, sizeof(board->pv));
			}
		}
	}

	if (!moved) {

		/*
		 * All moves in the move list illegal or move list empty.
		 * Evaluate this position or return the initial score
		 * if provided (not -INT_MAX).
		 */

		if (initial_score == -INT_MAX) {
			node_score.score = board_score(board, color, ply);
			(*evals)++;
		} else {
			node_score.score = initial_score;
		}
		node_score.move.from = node_score.move.to = 0;

		if (node_score.score >= beta) {
			node_score.type = node_cut;
		} else if (node_score.score > alpha) {
			node_score.type = node_pv;
		}
	}

	return node_score;
}

int node_get_score(board_t *b, int color, int search_depth, int alpha, int beta, int ply, int *evals)
{
	const ttable_entry *te = NULL;
	int move_count;
	int move_index;
	int score;
	int keep_searching = 1;
	int evals_under_node = 0;
	int i;
	move_t move_list[MOVE_LIST_MAX];
	search_tree_node_score_t node_score;

	if (ply < MAX_PLYS) {
		b->pv[ply].from = 0;
		b->pv[ply].to = 0;
	}

	te = ttable_lookup(ttable, get_zobrist_key(b, 0));
	if (te != NULL) {
		score = te->score.score;

		if (score <= CHECKMATE_SCORE+MAX_PLYS) {
			score += ply;
		} else if (score >= -CHECKMATE_SCORE-MAX_PLYS) {
			score -= ply;
		}
	}

	if (te != NULL && te->depth >= search_depth) {
		if (te->score.type == node_pv) {

			return score;

		} else if (te->score.type == node_cut) {
			/*
			 * score is a lower bound, it cannot be lower.
			 * Therefore if it higher than beta it is
			 * safe to return.
			 */
			if (score >= beta) {
				return score;
			}

			/*
			 * score did not exceed or meet beta,
			 * but since it's a lower bound we can
			 * raise alpha if alpha is lower
			 */
			alpha = MAX(alpha, score);

		} else {
			/*
			 * score is an upper bound, it cannot be higher.
			 * Therefore if it is lower than alpha it is
			 * safe to return.
			 */
			if (score <= alpha) {
				return score;
			}
		}
	}

	if (search_depth <= 0) {
		/*
		 * Proceed with a quiescence search.
		 * Get the standing pat score first.
		 */
		score = board_score(b, color, ply);
		(*evals)++;

		if (score >= beta) {
			node_score.move.from = node_score.move.to = 0;
			node_score.score = score;
			node_score.type = node_cut;
			node_score.repeat = 0;
			keep_searching = 0;
		} else {
			if (ply < MAX_PLYS) {
				alpha = MAX(alpha, score);
				/* Only search captures for quiescence search */
				move_count = get_capture_list(b, move_list, color);
			} else {
				/*
				 * If we're at a maximum ply depth then stop
				 * the search here.
				 */
				node_score.move.from = node_score.move.to = 0;
				node_score.score = score;
				node_score.type = score > alpha ? node_pv : node_all;
				node_score.repeat = 0;
				keep_searching = 0;
			}
		}
	} else {
		/* Proceed with a full depth search */

		int capture_count;
		int quiet_move_count;
		move_t *quiet_move_list;

		score = -INT_MAX;

		/* find captures first */
		capture_count = get_capture_list(b, move_list, color);

		/* set quiet move list to the end of the capture list */
		quiet_move_list = &move_list[capture_count];

		/* find the quiet moves */
		quiet_move_count = get_quiet_move_list(b, quiet_move_list, color);

		move_count = capture_count + quiet_move_count;

		/* prioritize killer moves in the quiet move list */
		for (i = 0; i < KILLER_MOVES_PER_PLY; i++) {
			if (in_list(quiet_move_list, quiet_move_count, killer_move[ply-1][i], &move_index)) {
				prioritize_move(quiet_move_list, move_index);
			}
		}
	}

	if (keep_searching) {
		if (search_depth > 0 || move_count > 0) {

			evals_under_node = *evals;

			if (te != NULL && (te->score.type == node_pv || te->score.type == node_cut)) {
				if (in_list(move_list, move_count, te->score.move, &move_index)) {
					prioritize_move(move_list, move_index);
				}
			}

			node_score = node_search(b, move_list, move_count, color, search_depth, score, alpha, beta, ply, evals);

			evals_under_node = *evals - evals_under_node;
		} else {
			/* in quiescence search and node is quiet */
			node_score.move.from = node_score.move.to = 0;
			node_score.score = score;
			node_score.type = score > alpha ? node_pv : node_all;
			node_score.repeat = 0;
		}
	}

	/* check if we found a "killer move" */
	if (node_score.type == node_cut &&
			node_score.move.to != node_score.move.from &&
			node_score.move.captured_piece == 0 && ply <= MAX_PLYS)
	{
		for (i = 0; i < KILLER_MOVES_PER_PLY; i++) {
			if (!move_cmp(killer_move[ply-1][i], node_score.move)) {
				/* If so, put it into the killer move list at the current ply,
				 * replacing the first entry that is different */
				killer_move[ply-1][i] = node_score.move;
				break;
			}
		}
	}

	if (!node_score.repeat) {
		ttable_entry new_te;
		new_te.key = get_zobrist_key(b, 0);
		new_te.score = node_score;
		if (node_score.score <= CHECKMATE_SCORE+MAX_PLYS) {
			new_te.score.score -= ply;
		} else if (node_score.score >= -CHECKMATE_SCORE-MAX_PLYS) {
			new_te.score.score += ply;
		}
		new_te.depth = search_depth;
		new_te.evals = evals_under_node;
		ttable_add(ttable, &new_te);
	}

	if (ply < MAX_PLYS && node_score.type == node_pv) {
		b->pv[ply] = node_score.move;
	}

	return node_score.score;
}

void print_pv(board_t *b)
{
	int i;

	fprintf(of, "PV: ");
	for (i = 0; i < MAX_PLYS; i++) {
		char curmove_s[6];

		if (b->pv[i].to == b->pv[i].from) {
			break;
		}

		move_t2mstr(curmove_s, b->pv[i]);
		fprintf(of, "%s ", curmove_s);
	}
	fprintf(of, "\n");
}

move_t find_best_move(board_t *b, move_t *move_list, int move_count, int search_depth)
{
	board_t b2;
	move_t best_move;
	int best_move_index = 0;
	int i, best_score, score, beta, evals = 0, last_eval = 0;
	char curmove_s[6];

	/* reset the killer move list at the beginning of each search */
	memset(killer_move, 0, sizeof(killer_move));

	for (int depth = 1; depth <= search_depth; depth++) {

		best_score = -INT_MAX;
		beta = INT_MAX;

		for (i=0; i<move_count; i++) {

			b2 = *b;
			if (move_piece(&b2, move_list[i], engine_color)) {
				// move leaves king in check
				continue;
			}

			/* check if this is a repeat move */
			if (get_zobrist_key(&b2, 4) == get_zobrist_key(&b2, 0)) {
				if (i < move_count-1) {
					/*
					 * if it is then consider it last.
					 * move it to the end of the list and try again
					 * at the same index.
					 */
					deprioritize_move(move_list, move_count, i);
					i--;
					continue;
				}
			}

			score = -node_get_score(&b2, !engine_color, depth-1, -beta, -best_score, 1, &evals);

			if (best_score < score && score < INT_MAX && i != 0) {
				score = -node_get_score(&b2, !engine_color, depth-1, -INT_MAX, -score, 1, &evals);
			}

			if (depth == search_depth) {
				move_t2mstr(curmove_s, move_list[i]);
				fprintf(of, "%s: %d .. %d evals\n", curmove_s, score, evals - last_eval);
				last_eval = evals;
				fflush(of);
			}
			if (score > best_score) {
				best_score = score;
				best_move = move_list[i];
				best_move_index = i;
				memcpy(b->pv, b2.pv, sizeof(b->pv));
			}

			beta = best_score+1;
		}
		prioritize_move(move_list, best_move_index);
	}

	b->pv[0] = best_move;

	print_pv(b);

	fprintf(of, "Total evals: %d\n", evals);
	fflush(of);

	return best_move;
}
