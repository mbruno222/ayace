# Copyright 2010 Michael Bruno
#
# This file is part of AYACE
#
# AYACE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# at your option) any later version.
#
# AYACE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AYACE.  If not, see <http://www.gnu.org/licenses/>.
#


TARGET = chess

CC = gcc
CFLAGS = -Ofast -std=gnu99 -Wall -march=native -flto
LIBS = -lpthread
LDFLAGS = $(LIBS) -flto -Ofast

SOURCES := . bitboard
CFILES := $(foreach dir, $(SOURCES), $(wildcard $(dir)/*.c))

OBJS = $(patsubst %.c,%.o,$(CFILES))

all: $(TARGET)

play: $(TARGET)
	xboard -debug -size medium -fcp ./$(TARGET)

playics: $(TARGET)
	xboard -zp -zippyAcceptOnly mattbruno -ics -icshost freechess.org -debug -size medium -fcp ./$(TARGET)

playself: $(TARGET)
	xboard -debug -size medium -mm -fcp ./$(TARGET) -scp ./$(TARGET)

playgnu: $(TARGET)
	xboard -debug -ponderNextMove false -size medium -mm -scp ./$(TARGET) -fcp '/usr/games/gnuchess -x'

playtoledo: $(TARGET)
	xboard -debug -size medium -mm -fcp ./$(TARGET) -scp ./toledo

clean:
	rm -f $(OBJS) $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@
	
	

