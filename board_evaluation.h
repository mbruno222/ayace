/*
 * board_evaluation.h
 *
 *  Created on: Oct 8, 2017
 *      Author: mbruno
 */

#ifndef BOARD_EVALUATION_H_
#define BOARD_EVALUATION_H_

#include "bitboard/bitboard.h"

#define CHECKMATE_SCORE (-500000)

int piece_value_sum(board_t *b, bitboard mask, int color);
int board_score(board_t *b, int color, int ply);

#endif /* BOARD_EVALUATION_H_ */
