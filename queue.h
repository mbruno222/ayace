/*  chatd - queue.h
*   Copyright (C) 2006  Michael Bruno
*
*   chatd is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   chatd is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with chatd; if not, write to the Free Software
*   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef QUEUE_H
#define QUEUE_H

#include <sys/time.h>
#ifdef __MACH__
#include <mach/semaphore.h>
#include <mach/task.h>

typedef semaphore_t sem_t;
#else
#include <semaphore.h>
#endif
#include <pthread.h>

#include "xboard_io.h"

/* List of messages */
typedef struct _list list;
struct _list {
	list *next;		/* pointer to next element in list */
	xboard_cmd_t cmd;
};

/* Linked list queue */
typedef struct _queue queue;
struct _queue {
	int max;	/* Boolean value, true if there is a max size */
	size_t size;	/* Current size of the queue */

	sem_t full;	/* Value is the number of used slots */
	sem_t empty;	/* Value is the number of empty slots if the
			   queue has a maximum */

	/* This could be a semaphore but I think
	   the mutex type is more appropriate */
	pthread_mutex_t lock;

	list *back;	/* pointer to back element in queue */
	list *front;	/* pointer to front element in queue */
};

queue *create_queue(int max);
int enqueue(queue *q, xboard_cmd_t cmd);
int dequeue(queue *q, xboard_cmd_t *cmd);
int free_queue(queue *q);

#endif

