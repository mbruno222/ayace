#ifndef HASH_H
#define HASH_H

#include <stdint.h>
#include "move_search.h"
#include "bitboard/bitboard.h"
#include "bitboard/bitboard_movelist.h"

typedef struct {
	uint64_t key;
//	bitboard b; //collision test
	search_tree_node_score_t score;
	int8_t depth;
	uint32_t evals;
} ttable_entry;

typedef struct {
	ttable_entry *table;
	int index_bit_length;
} ttable_t;

extern uint64_t hash_number_piece_position[12][64];
extern uint64_t hash_number_side_to_move;
extern uint64_t hash_number_castle_rights[4];
extern uint64_t hash_number_enpassant[8];

extern const int piece_to_hash_piece[8];

void init_hash_numbers(uint64_t seed);
ttable_t create_ttable(int index_bit_length);
void free_ttable(ttable_t ttable);
const ttable_entry *ttable_lookup(ttable_t ttable, uint64_t key);
void ttable_add(ttable_t ttable, const ttable_entry *new_te);
uint64_t get_zobrist_key(board_t *b, int when);
void gen_zobrist_key(board_t *b, int color);


#endif

