/* Copyright 2010 Michael Bruno
 *
 * This file is part of AYACE
 *
 * AYACE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * AYACE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AYACE.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MOVE_SEARCH_H
#define MOVE_SEARCH_H

#include "bitboard/bitboard.h"
#include "bitboard/bitboard_movelist.h"

typedef enum {
	node_pv,
	node_all,
	node_cut,
} search_tree_node_type_t;

typedef struct {
	int32_t score;
	move_t move; /* beta cutoff move or PV move */
	uint8_t repeat : 4;
	uint8_t type : 4;
} search_tree_node_score_t;

move_t find_best_move(board_t *b, move_t *move_list, int move_count, int search_depth);

#endif

