/* Copyright 2010 Michael Bruno
 *
 * This file is part of AYACE
 *
 * AYACE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * AYACE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AYACE.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "chess.h"

void quit(int snum)
{
	fclose(of);
	exit(0);
}

void emit_error(char *msg)
{
	fprintf(stderr, "%s: %s: %s\n", pname, msg, strerror(errno));
	quit(0);
}

ssize_t read_s(int fd, void *buf, size_t count)
{
	ssize_t ret;

	ret = read(fd, buf, count);

	if (ret != -1) {
		return ret;
	} else {
		emit_error("read failed");
		return -1;
	}
}

ssize_t write_s(int fd, const void *buf, size_t count)
{
	ssize_t ret;

	ret = write(fd, buf, count);

	if (ret != -1) {
		return ret;
	} else {
		emit_error("write failed");
		return -1;
	}
}

