/* Copyright 2010 Michael Bruno
 *
 * This file is part of AYACE
 *
 * AYACE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * AYACE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AYACE.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef XBOARD_IO_H
#define XBOARD_IO_H

typedef struct {
	enum {
		XBC_PROTOVER = 0,
		XBC_PING,
		XBC_NEW,
		XBC_USERMOVE,
		XBC_FORCE,
		XBC_BLACK,
		XBC_WHITE,
		XBC_GO,
		XBC_UNDO,
		XBC_EDIT,
		XBC_BK,
		XBC_QUIT
	} cmd;
	char arg[32];
} xboard_cmd_t;

#include "queue.h"

extern queue *xboard_commands;

int read_line(char *buf);
int check_command(char *in, char *cmd);
void send_command(char *cmd);
void xboard_io_init();

#endif

