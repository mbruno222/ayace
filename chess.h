/* Copyright 2010 Michael Bruno
 *
 * This file is part of AYACE
 *
 * AYACE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * AYACE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AYACE.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CHESS_H
#define CHESS_H

#include <stdio.h>
#include <pthread.h>
#include "hash.h"
#include "bitboard/bitboard_movelist.h"


extern char *pname;
extern FILE *of;
extern ttable_t ttable;
extern int engine_color;

extern int trait_search_depth;

void move_t2mstr(char *mstr, move_t m);

#endif

