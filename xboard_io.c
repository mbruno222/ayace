/* Copyright 2010 Michael Bruno
 *
 * This file is part of AYACE
 *
 * AYACE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * AYACE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AYACE.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include "chess.h"
#include "common.h"
#include "xboard_io.h"
#include "queue.h"

static pthread_mutex_t stdout_lock;
queue *xboard_commands;

int read_line(char *buf)
{
	int i;
	
	i = -1;
	do {
		i++;
		if (i == 128) i = 0;
		read_s(0, buf+i, 1);
	} while (buf[i] != '\n');

	buf[i] = '\0'; // replace the newline with a null

	return i; // return length (not including the newline)
}

static int valid_move(char *in)
{
	if (isalpha(in[0]) && isdigit(in[1]) && isalpha(in[2]) && isdigit(in[3])) {
		return 1;
	} else {
		return 0;
	}
}

int check_command(char *in, char *cmd)
{
	return !strncmp(in, cmd, strlen(cmd));
}

void send_command(char *cmd)
{
	pthread_mutex_lock(&stdout_lock);
	write_s(1, cmd, strlen(cmd));
	write_s(1, "\n", 1);
	pthread_mutex_unlock(&stdout_lock);

	fprintf(of, "to xboard: %s\n", cmd);
	fflush(of);
}

static void *xboard_listen(void *nothing)
{
	int len;
	int edit = 0;
	char inbuf[128];
	xboard_cmd_t xbd_cmd;

	while (1) {
		len = read_line(inbuf);

		fprintf(of, "%s\n%d----\n", inbuf,len);
		fflush(of);
		
		if (edit) {
			if (check_command(inbuf, ".")) {
				edit = 0;
			}
			xbd_cmd.cmd = XBC_EDIT;
			strcpy(xbd_cmd.arg, inbuf);
			enqueue(xboard_commands, xbd_cmd);
		} else {
			if (check_command(inbuf, "xboard")) {
			//	send_command("");
			} else if (check_command(inbuf, "protover")) {
				xbd_cmd.cmd = XBC_PROTOVER;
				strcpy(xbd_cmd.arg, inbuf+9);
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "ping")) {
				xbd_cmd.cmd = XBC_PING;
				strcpy(xbd_cmd.arg, inbuf+5);
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "usermove")) {
				xbd_cmd.cmd = XBC_USERMOVE;
				strcpy(xbd_cmd.arg, inbuf+9);
				enqueue(xboard_commands, xbd_cmd);
			} else if (valid_move(inbuf)) {
				xbd_cmd.cmd = XBC_USERMOVE;
				strcpy(xbd_cmd.arg, inbuf);
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "new")) {
				xbd_cmd.cmd = XBC_NEW;
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "force")) {
				xbd_cmd.cmd = XBC_FORCE;
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "black")) {
				xbd_cmd.cmd = XBC_BLACK;
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "white")) {
				xbd_cmd.cmd = XBC_WHITE;
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "go")) {
				xbd_cmd.cmd = XBC_GO;
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "undo")) {
				xbd_cmd.cmd = XBC_UNDO;
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "remove")) {
				xbd_cmd.cmd = XBC_UNDO;
				enqueue(xboard_commands, xbd_cmd);
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "edit")) {
				edit = 1;
				xbd_cmd.cmd = XBC_EDIT;
				xbd_cmd.arg[0] = '\0';
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "bk")) {
				xbd_cmd.cmd = XBC_BK;
				enqueue(xboard_commands, xbd_cmd);
			} else if (check_command(inbuf, "quit")) {
				quit(0);
			}
		}
	}

	return NULL;
}

void xboard_io_init()
{
	pthread_t id;

	xboard_commands = create_queue(0);

	if (pthread_create(&id, NULL, xboard_listen, NULL))
		emit_error("Error creating xboard_listen thread");

	pthread_mutex_init(&stdout_lock, NULL);

	pthread_detach(id);
}
