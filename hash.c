#include <stdlib.h>
#include <stdint.h>
#include "mt64.h"
#include "common.h"
#include "bitboard/bitboard.h"
#include "bitboard/bitboard_init.h"
#include "bitboard/bitboard_movelist.h"
#include "chess.h"
#include "hash.h"

uint64_t hash_number_piece_position[12][64];
uint64_t hash_number_side_to_move;
uint64_t hash_number_castle_rights[4];
uint64_t hash_number_enpassant[8];

#define BUCKETS 4

const int piece_to_hash_piece[8] = {0, 0, 1, 2, 0, 3, 4, 5};

void init_hash_numbers(uint64_t seed)
{
	int piece, i;

	init_genrand64(seed);

	for (piece = 0; piece < 12; piece++) {
		for (i = 0; i < 64; i++) {
			hash_number_piece_position[piece][i] = genrand64_int64();
		}
	}

	for (i = 0; i < 4; i++) {
		hash_number_castle_rights[i] = genrand64_int64();
	}


	for (i = 0; i < 8; i++) {
		hash_number_enpassant[i] = genrand64_int64();
	}

	hash_number_side_to_move = genrand64_int64();
}

ttable_t create_ttable(int index_bit_length)
{
	ttable_t ttable;

	ttable.index_bit_length = index_bit_length;
	ttable.table = calloc(1 << index_bit_length, sizeof(ttable_entry));

	return ttable;
}

void free_ttable(ttable_t ttable)
{
	free(ttable.table);
}

const ttable_entry *ttable_lookup(ttable_t ttable, uint64_t key)
{
	int i;
	const int shift = 64-ttable.index_bit_length;

	const ttable_entry *te = ttable.table + ((key >> shift) & ~(uint64_t)(BUCKETS-1));

	for (i = 0; i < BUCKETS; i++) {
		if (te[i].key == key) {
			return &te[i];
		}
	}
	return NULL;
}

void ttable_add(ttable_t ttable, const ttable_entry *new_te)
{
	const int shift = 64-ttable.index_bit_length;

	ttable_entry *te = ttable.table + ((new_te->key >> shift) & ~(uint64_t)(BUCKETS-1));

	/* always fill the first bucket with new positions */
	te[0] = *new_te;

	if (new_te->depth >= te[1].depth) {
		/*
		 * fill the second bucket if the depth of the new
		 * position is greater or equal to the old position.
		 */
		te[1] = *new_te;
	} else {
		/*
		 * fill the third and fourth buckets with the two
		 * positions that save the most evaluations
		 */
		int least_depth;
		if (te[2].evals < te[3].evals) {
			least_depth = 2;
		} else {
			least_depth = 3;
		}
		if (new_te->evals >= te[least_depth].evals) {
			te[least_depth] = *new_te;
		}
	}
}

/*
 * Gets the zobrist key of the board as it was "when" moves ago
 */
uint64_t get_zobrist_key(board_t *b, int when)
{
	return b->zobrist_key[(b->zobrist_key_index+when) & BITBOARD_KEY_HIST_INDEX_MASK];
}

 void gen_zobrist_key(board_t *b, int color)
{
	bitboard pb;
	int sq;
	uint64_t key = 0;

	key = color * hash_number_side_to_move;

	pb = b->color[WHITE].pawns;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[piece_to_hash_piece[PAWN]][sq];
	}
	pb = b->color[WHITE].knights;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[piece_to_hash_piece[KNIGHT]][sq];
	}
	pb = b->color[WHITE].bishops;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[piece_to_hash_piece[BISHOP]][sq];
	}
	pb = b->color[WHITE].rooks;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[piece_to_hash_piece[ROOK]][sq];
	}
	pb = b->color[WHITE].queens;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[piece_to_hash_piece[QUEEN]][sq];
	}
	pb = b->color[WHITE].king;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[piece_to_hash_piece[KING]][sq];
	}

	pb = b->color[BLACK].pawns;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[6+piece_to_hash_piece[PAWN]][sq];
	}
	pb = b->color[BLACK].knights;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[6+piece_to_hash_piece[KNIGHT]][sq];
	}
	pb = b->color[BLACK].bishops;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[6+piece_to_hash_piece[BISHOP]][sq];
	}
	pb = b->color[BLACK].rooks;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[6+piece_to_hash_piece[ROOK]][sq];
	}
	pb = b->color[BLACK].queens;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[6+piece_to_hash_piece[QUEEN]][sq];
	}
	pb = b->color[BLACK].king;
	while (pb) {
		sq = last_one_64(pb);
		pb = BITCLEAR(pb, sq);
		key ^= hash_number_piece_position[6+piece_to_hash_piece[KING]][sq];
	}

	// get the file on which the side to move can perform an en passant
	if (b->color[!color].pawn_shadow)
		key ^= hash_number_enpassant[last_one_64(b->color[!color].pawn_shadow) % 8];

	key ^= hash_number_castle_rights[0] * b->kscastle[WHITE];
	key ^= hash_number_castle_rights[1] * b->qscastle[WHITE];
	key ^= hash_number_castle_rights[2] * b->kscastle[BLACK];
	key ^= hash_number_castle_rights[3] * b->qscastle[BLACK];

	b->zobrist_key[b->zobrist_key_index] = key;
}


