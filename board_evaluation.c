/*
 * board_evaluation.c
 *
 *  Created on: Oct 3, 2017
 *      Author: mbruno
 */

#include "chess.h"
#include "board_evaluation.h"
#include "bitboard/bitboard.h"

// Position score tables taken from:
// http://www.chessbin.com/post/Chess-Board-Evaluation.aspx

static const int pawn_position_score[64] = {
	 0,  0,  0,  0,  0,  0,  0,  0,
	50, 50, 50, 50, 50, 50, 50, 50,
	10, 10, 20, 30, 30, 20, 10, 10,
	 5,  5, 10, 27, 27, 10,  5,  5,
	 0,  0,  0, 25, 25,  0,  0,  0,
	 5, -5,-10,  0,  0,-10, -5,  5,
	 5, 10, 10,-25,-25, 10, 10,  5,
	 0,  0,  0,  0,  0,  0,  0,  0
};

static const int knight_position_score[64] = {
	-50,-40,-30,-30,-30,-30,-40,-50,
	-40,-20,  0,  0,  0,  0,-20,-40,
	-30,  0, 10, 15, 15, 10,  0,-30,
	-30,  5, 15, 20, 20, 15,  5,-30,
	-30,  0, 15, 20, 20, 15,  0,-30,
	-30,  5, 10, 15, 15, 10,  5,-30,
	-40,-20,  0,  5,  5,  0,-20,-40,
	-50,-40,-20,-30,-30,-20,-40,-50
};

static const int bishop_position_score[64] = {
	-20,-10,-10,-10,-10,-10,-10,-20,
	-10,  0,  0,  0,  0,  0,  0,-10,
	-10,  0,  5, 10, 10,  5,  0,-10,
	-10,  5,  5, 10, 10,  5,  5,-10,
	-10,  0, 10, 10, 10, 10,  0,-10,
	-10, 10, 10, 10, 10, 10, 10,-10,
	-10,  5,  0,  0,  0,  0,  5,-10,
	-20,-10,-40,-10,-10,-40,-10,-20
};

static const int king_position_score[64] = {
	-30, -40, -40, -50, -50, -40, -40, -30,
	-30, -40, -40, -50, -50, -40, -40, -30,
	-30, -40, -40, -50, -50, -40, -40, -30,
	-30, -40, -40, -50, -50, -40, -40, -30,
	-20, -30, -30, -40, -40, -30, -30, -20,
	-10, -20, -20, -20, -20, -20, -20, -10,
	 20,  20,   0,   0,   0,   0,  20,  20,
	 20,  30,  10,   0,   0,  10,  30,  20
};

static int piece_score(int piece)
{
	switch (piece) {
	case PAWN:
		return 100;
	case KNIGHT:
		return 320;
	case BISHOP:
		return 333;
	case ROOK:
		return 510;
	case QUEEN:
		return 880;
	case KING:
		/* really doesn't matter, will never
		 * generate a board without both kings... */
		return 10000;
	default:
		return 0;
	}
}

int piece_value_sum(board_t *b, bitboard mask, int color)
{
	int value;

	value = __builtin_popcountll(b->color[color].pawns & mask) * piece_score(PAWN);
	value += __builtin_popcountll(b->color[color].knights & mask) * piece_score(KNIGHT);
	value += __builtin_popcountll(b->color[color].bishops & mask) * piece_score(BISHOP);
	value += __builtin_popcountll(b->color[color].rooks & mask) * piece_score(ROOK);
	value += __builtin_popcountll(b->color[color].queens & mask) * piece_score(QUEEN);
	//value += __builtin_popcountll(b->color[color].king & mask) * piece_score(KING);

	return value;
}

static int mate_score(board_t *board, int color, int ply)
{
	if (get_attacks_to(board, last_one_64(board->color[color].king), !color)) {
		/* checkmate.the further down the tree this is the better */
		return CHECKMATE_SCORE + ply;
	} else {
		/* stalemate */
		return 0;
	}
}

/*
 * todo: um. probably lots of stuff.
 */
int board_score(board_t *b, int color, int ply)
{
	bitboard all_pieces = b->all_pieces;
	bitboard king_moves[2] = {
			king_attacks[last_one_64(b->color[WHITE].king)] & ~b->color[WHITE].all,
			king_attacks[last_one_64(b->color[BLACK].king)] & ~b->color[BLACK].all
	};

	int score;

	int move_count;
	move_t move_list[MOVE_LIST_MAX];

	/* first we check if color has any legal moves */
	move_count = get_move_list(b, move_list, color);
	if (no_legal_moves(b, move_list, move_count, color)) {
		/* if not then we just return the mate score here */
		return mate_score(b, color, ply);
	}

	score = piece_value_sum(b, all_pieces, color) - piece_value_sum(b, all_pieces, !color);

	while (all_pieces) {

		bitboard moves;
		int sq = last_one_64(all_pieces);
		int piece_color = get_piece_color(b, sq);
		int piece = get_piece_type(b, sq, piece_color);
		int mobility_score = 0;
		int sm = (piece_color == color ? 1 : -1);

		// add score of position of the piece
		int position_index = piece_color == BLACK ? sq : 63-sq;
		switch (piece) {
		case PAWN:
			mobility_score += pawn_position_score[position_index];
			break;
		case KNIGHT:
			mobility_score += knight_position_score[position_index];
			break;
		case BISHOP:
			mobility_score += bishop_position_score[position_index];
			break;
		case KING:
			mobility_score += king_position_score[position_index];
			break;
		}

		moves = get_attacks_from(b, sq, piece, piece_color);

		// award pawns that are able to push forward
		if (piece == PAWN) {
			mobility_score += 2 * (get_pawn_pushes(b, sq, piece_color) != 0);
		}

		// award for each defended square
		mobility_score += 1 * __builtin_popcountll(moves & ~b->all_pieces);

		// award for each defended piece
		mobility_score += 2 * __builtin_popcountll(moves & b->color[piece_color].all);

		// award if forking
		mobility_score += 3 * (__builtin_popcountll(moves & b->color[!piece_color].all) > 1);

		// award threatening the king
		mobility_score += 4 * ((moves & b->color[!piece_color].king) != 0);

		// award for each spot attacked that the opponent's king can move to
		mobility_score += 4 * __builtin_popcountll(moves & king_moves[!piece_color]);

		score += mobility_score * sm;

		all_pieces = BITCLEAR(all_pieces, sq);
	}

	return score;
}
